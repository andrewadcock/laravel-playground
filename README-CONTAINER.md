## Usage

#### Set up repository
Clone the git repo
```
git clone git@gitlab.com:andrewadcock/laravel-docker-compose.git
```

CD into laravel-docker-container
```
cd laravel-docker-compose
```

#### Install composer dependencies
Locally
```
composer install
```
Or only in the container
```
docker run --rm -v $(pwd):/app composer install
```

#### Set up environment variables
Copy `.env.example` to `.env`
```
cp .env.example .env
```

#### Initialize environment
Start the docker containers
```
docker-compose up -d
```
Generate artisan key and save to cache
```
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan config:cache
```

#### Add MySQL User
Enter bash of database server
```
docker-compose exec db bash
```

Login to MySQL as root user
```
mysql -uroot -p
```

When prompted to enter a password enter
```
laravel
```

Create new user and grant permissions to laravel table
```
GRANT ALL ON laravel.* TO 'laraveluser'@'%' IDENTIFIED BY 'laravel';
```

Apply changes
```
FLUSH PRIVILEGES;
```

Exit mysql
```
EXIT;
```

Exit bash cli for database container
```
exit
```

View site at [http://localhost](http://localhost)


### Create Hosts Entry (optional)
Find the IP of your container
```
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' webserver
```
Edit your Hosts file 
Linux: 
```
sudo nano /etc/hosts
```
Add the following line to the bottom of your hosts file. Insert the IP you got from the first step, if different.
```
172.20.0.4 laravel.test
```
Visit the site at [http://laravel.test](http://laravel.test)

### Working with Laravel Commands
Test connection and add migrations table to DB
```
docker-compose exec app php artisan migrate
```

User Tinker (access to the database)
```
docker-compose exec app php artisan tinker
```

Test connection to newly created migration table
```
\DB::table('migrations')->get();
```

### Have Multiple Containers Running and want different urls?
Edit `{ROOT}/nginx/conf.d/app.conf`
Modify line 7 with your new domain name
Example: 
```
server_name laravel.test www.laravel.test;
```
becomes
```
server_name myproject.test www.myproject.test;
```

Make sure to repeat the hosts file update with the new domain.
