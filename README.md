# Laravel Playground

This is only for me, but I like to be thorough. This project is just to play around
with different Laravel concepts. None of this should be taken seriously and a lot
of it is going to be bad because it's the first time I've done it. Well, enjoy.


## Add-ons
[PHPCS - Laravel](https://github.com/emielmolenaar/phpcs-laravel)
Usage: `php vendor/bin/phpcs --standard=phpcs-laravel app/`
