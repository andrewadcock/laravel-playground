<div class="users">
    <h1>Users</h1>

    @if($users)
        <ul>
    @endif
    @foreach($users as $user)
        @if($user->store_id)
                    <li><strong>{{ $user->name }}</strong>: <a href="{{ $user->store->website }}" target="_blank">{{ ucfirst($user->store->name) }}</a></li>
        @else
            <li>{{ $user->name }}</li>
        @endif
    @endforeach
    @if($users)
        </ul>
    @endif
</div>
